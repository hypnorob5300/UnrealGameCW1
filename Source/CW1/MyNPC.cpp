// Fill out your copyright notice in the Description page of Project Settings.

#include "MyNPC.h"


// Sets default values
AMyNPC::AMyNPC(const class FObjectInitializer& PCIP)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DetectPlayerComp = PCIP.CreateDefaultSubobject<UDetectPlayerComponent>(this, TEXT("See Player Component"));
	DetectPlayerComp->OnEnterSight.AddDynamic(this, &AMyNPC::OnSeePlayer);
	DetectPlayerComp->OnExitSight.AddDynamic(this, &AMyNPC::OnUnseePlayer);
}

void AMyNPC::OnSeePlayer()
{
	GLog->Log("Player Entered sight");
}

void AMyNPC::OnUnseePlayer()
{
	GLog->Log("Player Exited sight");
}

// Called when the game starts or when spawned
void AMyNPC::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyNPC::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyNPC::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

