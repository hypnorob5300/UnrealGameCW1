// Fill out your copyright notice in the Description page of Project Settings.

#include "Door.h"
#include "UnrealMathUtility.h"
#include "Avatar.h"
#include "PlayerHUD.h"


// Sets default values
ADoor::ADoor(const class FObjectInitializer& PCIP)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Open = false;
	MoveTime = 1;
	messageTriggered = false;

	Mesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Mesh"));
	RootComponent = Mesh;
	DetectionBox = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("Detection Area"));
	DetectionBox->SetupAttachment(RootComponent);
	DetectionBox->OnComponentBeginOverlap.AddDynamic(this, &ADoor::OnBeginOverlap);
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();
	TargetPosition = GetActorLocation() + TargetPosition;
}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Open)
	{
		alpha += MoveTime * DeltaTime;
		FVector pos = GetActorLocation();
		SetActorLocation(
			FVector(
				FMath::Lerp(pos.X, TargetPosition.X, alpha),
				FMath::Lerp(pos.Y, TargetPosition.Y, alpha),
				FMath::Lerp(pos.Z, TargetPosition.Z, alpha)
				)
		);
		if (alpha >= 1) {
			Open = false;
			PrimaryActorTick.bCanEverTick = false;
		}
	}
}

void ADoor::OnBeginOverlap_Implementation(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Open || !PrimaryActorTick.bCanEverTick) return;
	AAvatar* avatar = Cast<AAvatar>(OtherActor);
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr) && (avatar != nullptr)) {
		if(avatar->inventory.Items.Contains(KeycardName)) Open = true;
		else if(!messageTriggered){
			APlayerController* PController = GetWorld()->GetFirstPlayerController();
			APlayerHUD* hud = Cast<APlayerHUD>(PController->GetHUD());
			hud->AddMessage(Message("You need a " + KeycardName + " to open the door", 6, FColor::Red));
			messageTriggered = true;
		}
	}
}

