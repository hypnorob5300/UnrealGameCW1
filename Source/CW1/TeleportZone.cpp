// Fill out your copyright notice in the Description page of Project Settings.

#include "TeleportZone.h"
#include "Avatar.h"


// Sets default values
ATeleportZone::ATeleportZone(const class FObjectInitializer& PCIP)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	TeleportTrigger = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("Teleport Trigger Area"));
	RootComponent = TeleportTrigger;
}

// Called when the game starts or when spawned
void ATeleportZone::BeginPlay()
{
	Super::BeginPlay();
	TeleportTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATeleportZone::OnOverlapBegin);
}

// Called every frame
void ATeleportZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleportZone::Teleport(FVector position, FRotator rotation, AAvatar* avatar)
{
	avatar->SetActorLocation(position);
	rotation.Pitch = 0;
	avatar->SetActorRotation(rotation);
}

void ATeleportZone::OnOverlapBegin_Implementation(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AAvatar* avatar = Cast<AAvatar>(OtherActor);
	if (avatar != nullptr) {
		Teleport(TargetLocation->GetActorLocation(), TargetLocation->GetActorRotation(), avatar);
	}
}

