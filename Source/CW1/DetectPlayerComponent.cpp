// Fill out your copyright notice in the Description page of Project Settings.

#include "DetectPlayerComponent.h"
#include "Kismet/GameplayStatics.h"


// Sets default values for this component's properties
UDetectPlayerComponent::UDetectPlayerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SightRange = 500;
	WasInSightLastTick = false;

	// ...
}


// Called when the game starts
void UDetectPlayerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UDetectPlayerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	APawn* player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (!player) return;

	FVector toPlayer = player->GetActorLocation() - GetOwner()->GetActorLocation();
	float distanceToPlayer = toPlayer.Size();

	bool isPlayerInSightRange = distanceToPlayer <= SightRange;

	if (isPlayerInSightRange && !WasInSightLastTick) OnEnterSight.Broadcast();

	if (!isPlayerInSightRange && WasInSightLastTick) OnExitSight.Broadcast();

	WasInSightLastTick = isPlayerInSightRange;
	// ...
}

