// Fill out your copyright notice in the Description page of Project Settings.

#include "Inventory.h"

Inventory::Inventory()
{
	MaxAmmo = 32;
	CurrentMagazine = 0;
	MagazineMax = 5;
	Reloading = false;
	ReloadDuration = 3;
	CurrentReloadTime = ReloadDuration;
}

Inventory::~Inventory()
{
}

void Inventory::Add(FString name, int32 amount, UTexture2D* icon)
{
	if (Items.Contains(name)) {
		Items[name].Quantity += amount;
	}
	else Items.Add(name, Item(name, amount, icon));
	if (name == "Bullets") {
		if (Items[name].Quantity > MaxAmmo) Items[name].Quantity = MaxAmmo;
		if (CurrentMagazine < 1)
		{
			Reload(true);
		}
	}
}

void Inventory::Remove(FString name)
{
	Items.Remove(name);
}

void Inventory::Remove(FString name, int32 amount)
{
	Items[name].Quantity -= amount;
	if (Items[name].Quantity < 1) Remove(name);
}

bool Inventory::HaveEnoughBullets()
{
	return Items.Contains("Bullets");
}

void Inventory::UseBullet()
{
	Remove("Bullets", 1);
	CurrentMagazine--;
	if (CurrentMagazine < 1) Reload(false);
}

void Inventory::Reload(bool force)
{
	if (HaveEnoughBullets() || force)
	{
		Reloading = true;
		CurrentReloadTime = ReloadDuration;
	}
}

void Inventory::ReloadTick(float DeltaTime)
{
	CurrentReloadTime -= DeltaTime;
	if (CurrentReloadTime < 0) {
		Reloading = false;
		if (Items["Bullets"].Quantity >= MagazineMax) CurrentMagazine = MagazineMax;
		else CurrentMagazine = Items["Bullets"].Quantity;
	}
}

