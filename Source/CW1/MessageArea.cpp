// Fill out your copyright notice in the Description page of Project Settings.

#include "MessageArea.h"
#include "PlayerHUD.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AMessageArea::AMessageArea(const class FObjectInitializer& PCIP)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerSphere = PCIP.CreateDefaultSubobject<USphereComponent>(this, TEXT("Trigger Sphere"));
	RootComponent = TriggerSphere;
	Triggered = false;
	TimeToShow = 5;
}

// Called when the game starts or when spawned
void AMessageArea::BeginPlay()
{
	Super::BeginPlay();
	TriggerSphere->OnComponentBeginOverlap.AddDynamic(this, &AMessageArea::OnOverlapBegin);
}

// Called every frame
void AMessageArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMessageArea::OnOverlapBegin_Implementation(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Triggered) return;
	AAvatar* avatar = Cast<AAvatar>(OtherActor);
	if (avatar != nullptr) {
		if (RequireItem) {
			AAvatar *avatar = Cast<AAvatar>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
			if (!avatar->inventory.Items.Contains(RequiredItemName)) return;
		}

		APlayerController* PController = GetWorld()->GetFirstPlayerController();
		APlayerHUD* hud = Cast<APlayerHUD>(PController->GetHUD());
		if (FullScreen) hud->ShowFullMessage(Message(HUDMessage, TimeToShow, MessageColour));
		else hud->AddMessage(Message(HUDMessage, TimeToShow, FColor::Yellow));
		Triggered = true;
	}
}