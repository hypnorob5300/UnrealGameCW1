// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DetectPlayerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEnterSight);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnExitSight);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CW1_API UDetectPlayerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDetectPlayerComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sight)
		float SightRange;

	UPROPERTY(BlueprintAssignable, Category = Sight)
	FOnEnterSight OnEnterSight;

	UPROPERTY(BlueprintAssignable, Category = Sight)
		FOnExitSight OnExitSight;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	bool WasInSightLastTick;
	
};
