// Fill out your copyright notice in the Description page of Project Settings.

#include "Bullet.h"


// Sets default values
ABullet::ABullet(const class FObjectInitializer& PCIP)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ProxSphere = PCIP.CreateDefaultSubobject<USphereComponent>(this, TEXT("Collision Sphere"));
	RootComponent = ProxSphere;
	ProxSphere->OnComponentBeginOverlap.AddDynamic(this, &ABullet::Prox);
	ProxSphere->OnComponentHit.AddDynamic(this, &ABullet::Hit);

	Mesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Bullet Mesh"));
	Mesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	ProxSphere->SetSimulatePhysics(true);
	ProxSphere->SetAllMassScale(0.001f);
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ABullet::SetFirer(FString s)
{
	Firer = s;
}

void ABullet::Prox_Implementation(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherComp != OtherActor->GetRootComponent()) return;
	if (OtherActor != nullptr && OtherActor->GetHumanReadableName() == Firer) return;

	OtherActor->TakeDamage(Damage, FDamageEvent(), NULL, this);

	Destroy();
}

void ABullet::Hit_Implementation(class UPrimitiveComponent* HitComp, AActor* HitActor, UPrimitiveComponent* c, FVector a, const FHitResult& d)
{
	Destroy();
}

