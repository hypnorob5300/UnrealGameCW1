// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enemy.h"
#include "PatrollingEnemy.generated.h"

/**
 * 
 */
UCLASS()
class CW1_API APatrollingEnemy : public AEnemy
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AActor*> PatrolPoints;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CurrentPoint = 0;
	bool Patrolling;

	void Tick(float DeltaTime) override;
	void Patrol(float DeltaTime);
	void GoToAndAttackPlayer(float DeltaTime);

	UFUNCTION(BlueprintNativeEvent, Category = Collision)
	void SightOverlap(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void BeginPlay() override;
};
