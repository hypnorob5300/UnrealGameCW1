// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Bullet.generated.h"

UCLASS()
class CW1_API ABullet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABullet(const class FObjectInitializer& PCIP);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Properties)
		float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Collision)
		USphereComponent* ProxSphere;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Collision)
		UStaticMeshComponent* Mesh;

	FString Firer;

	UFUNCTION(BlueprintNativeEvent, Category = Collision)
		void Prox(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION(BlueprintNativeEvent, Category = Collision)
		void Hit(class UPrimitiveComponent* HitComp, AActor* HitActor, UPrimitiveComponent* c, FVector a, const FHitResult& d);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void SetFirer(FString s);
	
	
};
