// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "Engine.h"
#include "MeleeWeapon.h"
#include "Kismet/GameplayStatics.h"

AEnemy::AEnemy(const class FObjectInitializer& PCIP)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Speed = 20;
	Health = 20;
	Experience = 0;
	BPLoot = NULL;
	BaseAttackDamage = 1;
	AttackTimeout = 1.5;
	TimeSinceLastStrike = 0;

	SightSphere = PCIP.CreateDefaultSubobject<USphereComponent>(this, TEXT("SightSphere"));
	SightSphere->SetupAttachment(RootComponent);

	AttackRangeSphere = PCIP.CreateDefaultSubobject<USphereComponent>(this, TEXT("AttackRangeSphere"));
	AttackRangeSphere->SetupAttachment(RootComponent);

	SightConeMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Sight Cone Mesh"));
	SightConeMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddMovementInput(Knockback, 1);
	Knockback *= 0.5f;

	APawn* avatar = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (!avatar) return;

	FVector playerPos = avatar->GetActorLocation();
	FVector toPlayer = playerPos - GetActorLocation();
	float distanceToPlayer = toPlayer.Size();
	if (distanceToPlayer < SightSphere->GetScaledSphereRadius())
	{
		FRotator toPlayerRotation = toPlayer.Rotation();
		toPlayerRotation.Pitch = 0;
		RootComponent->SetWorldRotation(toPlayerRotation);
	}

	if (distanceToPlayer < AttackRangeSphere->GetScaledSphereRadius()) {
		if (!TimeSinceLastStrike) {
			Attack(avatar);
		}

		TimeSinceLastStrike += DeltaTime;
		if (TimeSinceLastStrike > AttackTimeout) TimeSinceLastStrike = 0;
	}
	else {
		AddMovementInput(toPlayer, Speed * DeltaTime);
	}
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::Attack(AActor* target)
{
	if (BPBullet)
	{
		FVector forward = GetActorForwardVector();
		FVector nozzle = GetMesh()->GetBoneLocation("index_03_l");
		nozzle += forward * 30;

		FVector toOpponent = target->GetActorLocation() - nozzle;
		toOpponent.Normalize();

		ABullet* bullet = GetWorld()->SpawnActor<ABullet>(BPBullet, nozzle, RootComponent->GetComponentRotation());

		if (bullet != nullptr) {
			bullet->SetFirer(GetHumanReadableName());
			bullet->ProxSphere->AddImpulse(toOpponent * BulletLaunchImpulse);
		}
	}
}

void AEnemy::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (BPMeleeWeapon) {
		MeleeWeapon = GetWorld()->SpawnActor<AMeleeWeapon>(BPMeleeWeapon, FVector(0), FRotator(0));
		if (MeleeWeapon) {
			MeleeWeapon->WeaponHolder = this;
			const USkeletalMeshSocket* socket = GetMesh()->GetSocketByName("RightHandSocket");
			socket->AttachActor(MeleeWeapon, GetMesh());
		}
	}
}

float AEnemy::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	Knockback = GetActorLocation() - DamageCauser->GetActorLocation();
	Knockback.Normalize();
	Knockback *= Damage * 500;

	Health -= Damage;
	if (Health < 0)
	{
		Destroy();
	}

	return Damage;
}

bool AEnemy::IsInAttackRangeOfPlayer()
{
	APawn* avatar = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (!avatar) return false;

	FVector playerPos = avatar->GetActorLocation();
	FVector toPlayer = playerPos - GetActorLocation();
	float distanceToPlayer = toPlayer.Size();

	return distanceToPlayer <= AttackRangeSphere->GetScaledSphereRadius();
}

void AEnemy::Slash()
{
	if (MeleeWeapon) {
		MeleeWeapon->Swing();
	}
}

void AEnemy::EndSlash()
{
	if (MeleeWeapon) {
		MeleeWeapon->Rest();
	}
}


