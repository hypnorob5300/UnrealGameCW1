// Fill out your copyright notice in the Description page of Project Settings.

#include "MeleeWeapon.h"


// Sets default values
AMeleeWeapon::AMeleeWeapon(const class FObjectInitializer& PCIP)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Damage = 1;
	Swinging = false;
	WeaponHolder = NULL;

	Mesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, "Mesh");
	RootComponent = Mesh;

	proxBox = PCIP.CreateDefaultSubobject<UBoxComponent>(this, "Box");
	proxBox->SetupAttachment(RootComponent);
	proxBox->OnComponentBeginOverlap.AddDynamic(this, &AMeleeWeapon::Prox);
}

void AMeleeWeapon::Prox_Implementation(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor == WeaponHolder) return;

	if (!Swinging) return;

	if (OtherComp != OtherActor->GetRootComponent()) return;

	if (thingsHit.Contains(OtherActor)) return;

	OtherActor->TakeDamage(Damage, FDamageEvent(), NULL, this);

	thingsHit.Add(OtherActor);
}

void AMeleeWeapon::Swing()
{
	thingsHit.Empty();
	Swinging = true;
}

void AMeleeWeapon::Rest()
{
	Swinging = false;
}

// Called when the game starts or when spawned
void AMeleeWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMeleeWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

