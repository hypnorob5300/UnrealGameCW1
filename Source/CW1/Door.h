// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Door.generated.h"

UCLASS()
class CW1_API ADoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADoor(const class FObjectInitializer& PCIP);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool Open;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FVector TargetPosition;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float MoveTime;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString KeycardName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* Mesh;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UBoxComponent* DetectionBox;
	
	UFUNCTION(BlueprintNativeEvent, Category = Collision)
		void OnBeginOverlap(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:
	float alpha;
	bool messageTriggered;
};
