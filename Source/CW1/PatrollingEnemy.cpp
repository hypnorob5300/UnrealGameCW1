// Fill out your copyright notice in the Description page of Project Settings.

#include "PatrollingEnemy.h"
#include "Kismet/GameplayStatics.h"
#include "Avatar.h"

void APatrollingEnemy::Tick(float DeltaTime)
{
	if(Patrolling) Patrol(DeltaTime);
	else GoToAndAttackPlayer(DeltaTime);

	AddMovementInput(Knockback, 1);
	Knockback *= 0.5f;
}

void APatrollingEnemy::Patrol(float DeltaTime)
{
	FVector target = PatrolPoints[CurrentPoint]->GetActorLocation();
	FVector toTarget = target - GetActorLocation();

	float distanceToTarget = toTarget.Size();
	if (distanceToTarget > 120) {
		//Go to target
		//Set rotation
		FRotator targetRot = toTarget.Rotation();
		targetRot.Pitch = 0;
		RootComponent->SetWorldRotation(targetRot);
		//Add Input
		AddMovementInput(toTarget, Speed * DeltaTime);
	}
	else {
		//Change target
		CurrentPoint++;
		if (CurrentPoint > PatrolPoints.Num() - 1)
		{
			CurrentPoint = 0;
		}
	}
}

void APatrollingEnemy::GoToAndAttackPlayer(float DeltaTime)
{
	APawn* avatar = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (!avatar) return;

	FVector playerPos = avatar->GetActorLocation();
	FVector toPlayer = playerPos - GetActorLocation();
	float distanceToPlayer = toPlayer.Size();

	FRotator toPlayerRotation = toPlayer.Rotation();
	toPlayerRotation.Pitch = 0;
	RootComponent->SetWorldRotation(toPlayerRotation);

	if (distanceToPlayer < AttackRangeSphere->GetScaledSphereRadius()) {
		if (!TimeSinceLastStrike) {
			Attack(avatar);
		}

		TimeSinceLastStrike += DeltaTime;
		if (TimeSinceLastStrike > AttackTimeout) TimeSinceLastStrike = 0;
	}
	else {
		AddMovementInput(toPlayer, Speed * DeltaTime);
	}
}

void APatrollingEnemy::BeginPlay()
{
	Super::BeginPlay();

	Patrolling = true;
	SightConeMesh->OnComponentBeginOverlap.AddDynamic(this, &APatrollingEnemy::SightOverlap);
}

void APatrollingEnemy::SightOverlap_Implementation(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!Patrolling) return;
	AAvatar* avatar = Cast<AAvatar>(OtherActor);
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr) && (avatar != nullptr)) {
		FHitResult hit;
		//Ensure the patrolling enemy also has line of sight to the player.
		//Prevents ending a patrol through a wall.
		if (!GetWorld()->LineTraceSingleByObjectType(hit, GetActorLocation() + GetActorForwardVector() * 30, 
			avatar->GetActorLocation(), FCollisionObjectQueryParams(ECC_WorldStatic)))
		{
			Patrolling = false;
		}
	}
}