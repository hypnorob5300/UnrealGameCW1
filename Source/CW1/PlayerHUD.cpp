// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerHUD.h"
#include "GameFramework/Actor.h"
#include "GameFramework/HUD.h"
#include "Engine/Canvas.h"
#include "Kismet/GameplayStatics.h"
#include "Avatar.h"

void APlayerHUD::DrawHUD()
{
	Super::DrawHUD();

	canvx = Canvas->SizeX;
	canvy = Canvas->SizeY;

	AAvatar *avatar = Cast<AAvatar>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

	DrawMessages();
	DrawHealthbar(avatar);
	DrawAmmo(avatar);
	if(DoDrawInventory) DrawInventory(avatar);
	if (DoDrawFullMessage) DrawFullMessage(avatar);
}

void APlayerHUD::AddMessage(Message msg)
{
	messages.Add(msg);
}

void APlayerHUD::ShowFullMessage(Message message)
{
	fullScreenMessage = message;
	fullMessageAlpha = 0;
	if (alphaMultiplier < 0) alphaMultiplier *= -1;
	DoDrawFullMessage = true;
	AAvatar *avatar = Cast<AAvatar>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	avatar->AcceptInput = false;
}

void APlayerHUD::DrawMessages()
{
	for(int i = messages.Num() -1; i >= 0; i--)
	{
		// draw the background box the right size for the message
		float ow, oh, pad = 10.f;
		GetTextSize(messages[i].message, ow, oh, hudFont, 1.f);

		float messageH = oh + 2 * pad;
		float x = canvx / 3, y = i * messageH;

		DrawRect(FLinearColor::Black, x, y, canvx / 3, messageH);
		DrawText(messages[i].message, messages[i].frontColor, x + 30 + pad, y + pad, hudFont);
		if(messages[i].tex != nullptr) DrawTexture(messages[i].tex, x, y, messageH, messageH, canvx, 0, 1, 1);

		// reduce lifetime by the time that passed since last frame.
		messages[i].time -= GetWorld()->GetDeltaSeconds();
		// if the message's time is up, remove it
		if (messages[i].time < 0)
		{
			messages.RemoveAt(i);
		}
	}
}

void APlayerHUD::DrawHealthbar(AAvatar* avatar)
{
	float barWidth = canvx / 3, barHeight = barWidth / 20;
	float padding = 5;
	float percHp = avatar->Health / avatar->MaxHealth;

	//Background
	DrawRect(FLinearColor::Black, 0, 
		canvy - barHeight, barWidth, barHeight);
	//Foreground health bar
	//FLinearColor (1 - percHp, percHp, 0, 1)
	DrawRect(FLinearColor::Red, 0 + padding, 
		canvy - barHeight + padding, (barWidth - padding * 2) * percHp, barHeight - padding * 2);
	//Health Text
	DrawText("Health:", FLinearColor::White, 5, (canvy - barHeight) - 20, hudFont, 1.5);
}

void APlayerHUD::DrawAmmo(AAvatar* avatar)
{
	if (avatar->inventory.Reloading) {
		DrawText("Reloading...", FLinearColor::White, canvx - 150, canvy - 40, hudFont, 2);
	}
	else if (avatar->inventory.Items.Contains("Bullets")) {
		int32 ammo = avatar->inventory.CurrentMagazine;
		int32 allAmmo = avatar->inventory.Items["Bullets"].Quantity - (avatar->inventory.MagazineMax - (avatar->inventory.MagazineMax - ammo));
		//FString ptext = FString::Format(ptext, , FString::FromInt(avatar->inventory.MaxAmmo));
		FString ptext = "Ammo: " + FString::FromInt(ammo) + "/" + FString::FromInt(allAmmo);
		DrawText(ptext, FLinearColor::White, canvx - 150, canvy - 40, hudFont, 2);
	}
	else {
		DrawText("No Ammo", FLinearColor::White, canvx - 150, canvy - 40, hudFont, 2);
	}
}

void APlayerHUD::DrawInventory(AAvatar* avatar)
{
	float width = canvx / 3;
	float height = 40;
	float startx = (canvx / 2) - (width / 2);
	float starty = (canvy / 2) - (avatar->inventory.Items.Num() * height) / 2;
	float padding = 5;
	if (avatar->inventory.Items.Num() > 0)
	{
		for (TMap<FString, Item>::TIterator item = avatar->inventory.Items.CreateIterator(); item; ++item)
		{
			DrawRect(FLinearColor(0, 0, 0, 0.7), startx, starty, width, height);
			DrawText(item->Key + ": x" + FString::FromInt(item->Value.Quantity), FLinearColor::White, startx + 10, starty + (height / 4), hudFont);
			DrawTexture(item->Value.Icon, startx + width - height - padding, starty, height, height, canvx, 0, 1, 1);
			starty += height + padding;
		}
	}
	else
	{
		//Inventory is empty
	}
}

void APlayerHUD::DrawFullMessage(AAvatar* avatar)
{
	DrawRect(FLinearColor(0, 0, 0, fullMessageAlpha), 0, 0, canvx, canvy);
	DrawText(fullScreenMessage.message, FLinearColor(fullScreenMessage.frontColor.R, fullScreenMessage.frontColor.G, fullScreenMessage.frontColor.B, fullMessageAlpha), (canvx / 2) - 80, canvy / 2, hudFont, 1.3);
	fullMessageAlpha += GetWorld()->GetDeltaSeconds() * alphaMultiplier;
	if (fullMessageAlpha > fullScreenMessage.time + 1 && alphaMultiplier > 0) alphaMultiplier = -alphaMultiplier;
	if (alphaMultiplier < 0 && fullMessageAlpha < 0) {
		DoDrawFullMessage = false;
		avatar->AcceptInput = true;
	}
}
