// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/Texture2D.h"
#include "CoreMinimal.h"

struct Item {

public:
	FString Name;
	int32 Quantity;
	UTexture2D* Icon;

	Item(FString name, int32 quantity, UTexture2D* icon)
	{
		Name = name;
		Quantity = quantity;
		Icon = icon;
	}
};

class CW1_API Inventory
{
public:
	Inventory();
	~Inventory();

	void Add(FString name, int32 amount, UTexture2D* icon);
	void Remove(FString);
	void Remove(FString, int32);
	bool HaveEnoughBullets();
	void UseBullet();
	void Reload(bool force);
	void ReloadTick(float DeltaTime);

	int32 MaxAmmo;
	int32 CurrentMagazine;
	int32 MagazineMax;
	bool Reloading;
	float ReloadDuration;
	float CurrentReloadTime;
	
	TMap<FString, Item> Items;

};
