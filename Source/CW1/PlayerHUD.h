// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Avatar.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */

struct Message{
	FString message;
	float time;
	FColor frontColor;
	UTexture2D* tex;

	Message()
	{
		// Set the default time.
		time = 5.f;
		frontColor = FColor::White;
	}
	Message(FString iMessage, float iTime, FColor iFrontColor)
	{
		message = iMessage;
		time = iTime;
		frontColor = iFrontColor;
		tex = nullptr;
	}
	Message(FString iMessage, float iTime, FColor iFrontColor, UTexture2D* iTex)
	{
		message = iMessage;
		time = iTime;
		frontColor = iFrontColor;
		tex = iTex;
	}
};

UCLASS()
class CW1_API APlayerHUD : public AHUD
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUDFont)
		UFont *hudFont;
	TArray<Message> messages;
	float canvx, canvy;

	virtual void DrawHUD() override;
	void AddMessage(Message msg);
	void ShowFullMessage(Message message);
	bool DoDrawInventory = false;

private:
	bool DoDrawFullMessage = false;
	float fullMessageAlpha = 0;
	float alphaMultiplier = 1.5;

	Message fullScreenMessage;

	void DrawMessages();
	void DrawHealthbar(AAvatar* avatar);
	void DrawAmmo(AAvatar* avatar);
	void DrawInventory(AAvatar* avatar);
	void DrawFullMessage(AAvatar* avatar);
};
