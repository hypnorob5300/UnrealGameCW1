// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Avatar.h"
#include "TeleportZone.generated.h"

UCLASS()
class CW1_API ATeleportZone : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATeleportZone(const class FObjectInitializer& PCIP);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* TeleportTrigger;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* TargetLocation;
	
	UFUNCTION(BlueprintNativeEvent, Category = Colision)
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void Teleport(FVector position, FRotator rotation, AAvatar* avatar);
};
