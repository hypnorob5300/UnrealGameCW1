// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "MessageArea.generated.h"

UCLASS()
class CW1_API AMessageArea : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMessageArea(const class FObjectInitializer& PCIP);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent, Category = Colision)
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* TriggerSphere;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString HUDMessage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float TimeToShow;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool FullScreen = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FColor MessageColour = FColor::Yellow;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool RequireItem = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString RequiredItemName;

private:
	bool Triggered;

	
};
