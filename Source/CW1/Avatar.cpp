// Fill out your copyright notice in the Description page of Project Settings.

#include "Avatar.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "PlayerHUD.h"
#include "Pickup.h"
#include "Camera/CameraComponent.h"

// Sets default values
AAvatar::AAvatar()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create the Collection Sphere
	CollectionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollectionSphere"));
	CollectionSphere->SetupAttachment(RootComponent);
	CollectionSphere->SetSphereRadius(200.f);

	Health = 100;
	MaxHealth = Health;
	AcceptInput = true;
}

// Called when the game starts or when spawned
void AAvatar::BeginPlay()
{
	Super::BeginPlay();
	CollectionSphere->OnComponentBeginOverlap.AddDynamic(this, &AAvatar::OnOverlapBegin);
}

// Called every frame
void AAvatar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddMovementInput(Knockback, 1);
	Knockback *= 0.5f;

	//Reload timer
	if (inventory.Reloading) inventory.ReloadTick(DeltaTime);
}

// Called to bind functionality to input
void AAvatar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	InputComponent->BindAxis("Forward", this, &AAvatar::MoveForward);
	InputComponent->BindAxis("Strafe", this, &AAvatar::MoveRight);
	InputComponent->BindAxis("Yaw", this, &AAvatar::Yaw);
	InputComponent->BindAxis("Pitch", this, &AAvatar::Pitch);
	InputComponent->BindAction("Inventory", IE_Pressed, this, &AAvatar::ToggleInventory);
	InputComponent->BindAction("Attack", IE_Pressed, this, &AAvatar::Attack);
}

void AAvatar::MoveForward(float amount)
{
	if (!AcceptInput) return;
	if (Controller && amount)
	{
		FVector fwd = GetActorForwardVector();
		
		AddMovementInput(fwd, amount);
	}
}

void AAvatar::MoveRight(float amount)
{
	if (!AcceptInput) return;
	if (Controller && amount)
	{
		FVector right = GetActorRightVector();
		AddMovementInput(right, amount);
	}
}

void AAvatar::Yaw(float amount)
{
	if (!AcceptInput) return;
	AddControllerYawInput(200.f * amount * GetWorld()->GetDeltaSeconds());
}

void AAvatar::Pitch(float amount)
{
	if (!AcceptInput) return;
	AddControllerPitchInput(200.f * amount * GetWorld()->GetDeltaSeconds());
}

void AAvatar::OnOverlapBegin_Implementation(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APickup *pickup = Cast<APickup>(OtherActor);
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr) && (pickup != nullptr)) {
		if(!pickup->WasPickedUp) Pickup(pickup);
	}
}

float AAvatar::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	if (!AcceptInput) return 0;
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	Knockback = GetActorLocation() - DamageCauser->GetActorLocation();
	Knockback.Normalize();
	Knockback *= Damage * 500;
	
	Health -= Damage;
	if(Health < 0)
	{
		Die();
	}

	return Damage;
}

void AAvatar::Pickup(APickup *item)
{
	inventory.Add(item->Name, item->Quantity, item->Icon);
	item->WasPickedUp = true;
	item->Destroy();

	APlayerController* PController = GetWorld()-> GetFirstPlayerController();
	APlayerHUD* hud = Cast<APlayerHUD>(PController-> GetHUD());
	hud->AddMessage(Message("Picked up " + item->Name + " (x" + FString::FromInt(item->Quantity) + ")", 5.f, FColor::White, item->Icon));
}

void AAvatar::Die()
{
	AcceptInput = false;
	APlayerController* PController = GetWorld()->GetFirstPlayerController();
	APlayerHUD* hud = Cast<APlayerHUD>(PController->GetHUD());
	hud->ShowFullMessage(Message("You Died!", 9999999, FColor::Red));
}

void AAvatar::ToggleInventory()
{
	APlayerController* PController = GetWorld()->GetFirstPlayerController();
	APlayerHUD* hud = Cast<APlayerHUD>(PController->GetHUD());
	hud->DoDrawInventory = !hud->DoDrawInventory;
}

void AAvatar::Attack()
{
	if (BPBullet && !inventory.Reloading && inventory.HaveEnoughBullets())
	{
		FVector forward = GetActorForwardVector() * 120;

		ABullet* bullet = GetWorld()->SpawnActor<ABullet>(BPBullet, GetActorLocation() + forward, RootComponent->GetComponentRotation());

		if (bullet != nullptr) {
			bullet->Firer = GetHumanReadableName();
			bullet->ProxSphere->AddImpulse(forward * BulletLaunchImpulse);
			inventory.UseBullet();
		}
	}
}
